<?php

namespace Database\Factories;


use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{

    public function definition()
    {
        $title = fake()->sentence();
        return [
            'title' => $title,
            'body' => fake()->text(500),
            'slug' => Str::slug($title),
            'user_id' => User::all()->random()->id
        ];
    }
}
