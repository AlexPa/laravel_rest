<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Database\Seeders\PostSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class ApiPostsTest extends TestCase
{
    private ?string $token = null;
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed([
            UserSeeder::class,
            PostSeeder::class
        ]);
        if ($this->token === null) {
            $email = UserSeeder::TEST_EMAIL;
            $password = "password";

            $response = $this->post('/auth/token', [
                'email' => $email,
                'password' => $password
            ], [
                "Accept: application/json, */*; q=0.01",
                "Content-Type: application/x-www-form-urlencoded"
            ]);

            $response->assertStatus(Response::HTTP_OK);
            $this->token = $response->content();
        }
    }


    private function getToken(): string
    {
        return $this->token;
    }

    public function test_index(): void
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->get('/api/posts');

        $response->assertHeader("content-type", "application/json");
        $response->assertHeader("x-ratelimit-limit");
        $response->assertHeader("x-ratelimit-remaining");
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                ]
            ],
            'links',
            'meta'
        ]);
        $response->assertJsonCount(config("my.perPage"), 'data');
    }

    public function test_index_without_auth_header(): void
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->get('/api/posts');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_index_as_post_request()
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->post('/api/posts');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonStructure([
            'errors',
            'message',
        ]);
    }

    public function test_index_as_head_request()
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->head('/api/posts');
        $response->assertNoContent(Response::HTTP_OK);
    }

    public function test_index_as_options_request()
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->options('/api/posts');
        $response->assertHeader("allow", "GET,HEAD,POST");
    }

    public function test_show(): void
    {
        $post = Post::query()->first();
        $this->assertInstanceOf(Post::class, $post);
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->get('/api/posts/' . $post->id);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data'
        ]);
    }

    public function test_show_non_exists_id(): void
    {
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->get('/api/posts/0');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function test_show_as_post_request(): void
    {
        $post = Post::query()->first();
        $this->assertInstanceOf(Post::class, $post);
        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->post('/api/posts/' . $post->id);

        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function test_store(): void
    {
        $user = User::query()->first();
        $this->assertInstanceOf(User::class, $user);

        $title = fake()->sentence();
        $slug = Str::slug($title);

        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->withHeader("Content-type:", "application/json")
            ->post('/api/posts', [
                'title' => $title,
                'body' => fake()->text(500),
                'user_id' => $user->id
            ], []);
        $response->assertStatus(Response::HTTP_CREATED);

        $post = Post::query()->orderBy('id', 'desc')->first();
        $this->assertInstanceOf(Post::class, $post);

        $this->assertEquals($post->slug, $slug);
    }

    public function test_update(): void
    {
        $post = Post::query()->first();
        $this->assertInstanceOf(Post::class, $post);

        $title = fake()->sentence();
        $slug = Str::slug($title);

        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->withHeader("Content-type:", "application/json")
            ->put('/api/posts/' . $post->id, [
                'title' => $title,
                'body' => $post->body,
                'user_id' => $post->user_id
            ]);
        $post = Post::query()->where('id', $post->id)->first();
        $this->assertInstanceOf(Post::class, $post);

        $this->assertEquals($post->slug, $slug);
        $this->assertEquals($post->title, $title);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $response->assertNoContent();
    }

    public function test_delete(): void
    {
        $post = Post::query()->first();
        $this->assertInstanceOf(Post::class, $post);

        $response = $this
            ->withHeader("Accept", "application/json, */*; q=0.01")
            ->withHeader("Authorization", "Bearer " . $this->getToken())
            ->delete('/api/posts/' . $post->id);

        $post = Post::query()->where('id', $post->id)->first();
        $this->assertNull($post);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $response->assertNoContent();
    }
}
