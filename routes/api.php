<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware(['auth:sanctum', 'cors'])->get('/posts', [\App\Http\Controllers\PostController::class, 'index']);
Route::middleware(['auth:sanctum', 'cors'])->get('/posts/{id}', [\App\Http\Controllers\PostController::class, 'show']);
Route::middleware(['auth:sanctum', 'cors'])->post('/posts', [\App\Http\Controllers\PostController::class, 'store']);
Route::middleware(['auth:sanctum', 'cors'])->put('/posts/{id}', [\App\Http\Controllers\PostController::class, 'update']);
Route::middleware(['auth:sanctum', 'cors'])->delete('/posts/{id}', [\App\Http\Controllers\PostController::class, 'delete']);
