<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUploadFileStoreRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    public function index(Request $request)
    {
        return new PostCollection(Post::paginate(config("my.perPage")));
    }

    public function show(Request $request)
    {
        return new PostResource(Post::findOrFail($request->id));
    }

    public function store(PostStoreRequest $request)
    {
        Post::create($request->all());
        return response()->noContent(Response::HTTP_CREATED);
    }

    public function update(PostStoreRequest $request)
    {
        $post = Post::findOrFail($request->id);
        $post->update($request->all());
        return response()->noContent(Response::HTTP_NO_CONTENT);
    }

    public function delete(Request $request)
    {
        $res = Post::where('id', $request->id)->delete();
        return response()->noContent(Response::HTTP_NO_CONTENT);
    }
}
