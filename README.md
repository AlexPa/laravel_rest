# REST API LARAVEL 10



## First of all:
### 1. Get token
Request:
```
curl -X 'POST' \
  'http://laravel.alexpa.site/auth/token' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'email=test%40mail.test&password=password'
```
Response:
```
17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a
```
## And then:

### 2. All posts:
Request:
```
curl -X 'GET' \
  'http://laravel.alexpa.site/api/posts' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer 17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a'
```
Response:
```
{
  "data": [
    {
      "title": "Animi ducimus tenetur id blanditiis.",
      "id": 1,
      "body": "Ut iure temporibus magni debitis sequi rem. Recusandae iste laboriosam dolore omnis eum veritatis unde dolor. Perspiciatis suscipit sunt adipisci in fugit est. Illo itaque reiciendis ullam explicabo sed temporibus sit. Quisquam et voluptatem numquam. Sed et quasi nihil repellendus doloremque et. Labore rerum quis voluptate nisi. Tenetur ut dolores in laboriosam corporis qui voluptate. Odio accusamus at nesciunt minima beatae. Amet occaecati est tempora dolorem.",
      "user_id": 1,
      "file": null
    },
    ...
  ]
  ...
}
```

### 3. Post by id:
Request:
```
curl -X 'GET' \
  'http://laravel.alexpa.site/api/posts/3' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer 17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a'
```
Response:
```
{
  "data": {
    "title": "Voluptatibus velit vel sapiente nostrum adipisci unde.",
    "id": 3,
    "body": "Molestias et quia tenetur numquam rerum doloremque. Asperiores blanditiis asperiores perferendis ut illo cum ut. Adipisci itaque nostrum qui nemo maiores sunt vel. Rerum accusantium et dolor facere. Earum sit et dicta fugit. A quo et dolor suscipit impedit voluptates voluptate sed. Magnam blanditiis vitae voluptatum beatae.",
    "user_id": 1,
    "file": null
  }
}
```

### 4. Create post:
Request:
```
curl -X 'POST' \
  'http://laravel.alexpa.site/api/posts' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer 17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a' \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "Test post",
  "body": "Body of post",
  "user_id": 1
}'
```
Response:
```
```

### 5. Update post:
Request:
```
curl -X 'PUT' \
  'http://laravel.alexpa.site/api/posts/3' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer 17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a' \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "Updated post",
  "body": "Updated body",
  "user_id": 1
}'
```
Response:
```
```

### 6. Delete post:
Request:
```
curl -X 'DELETE' \
  'http://laravel.alexpa.site/api/posts/4' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer 17|pc1oZgdD6wdBpW5TWh52IKQkV31BcmWhVIfDcM2a'
```
Response:
```
```
